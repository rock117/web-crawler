name := "web-crawler"

scalaVersion := "2.11.2"

resolvers += "codahale" at "http://repo.codahale.com"
 
resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

unmanagedBase := baseDirectory.value / "akka-lib"
 
libraryDependencies += "commons-httpclient" % "commons-httpclient" % "3.1"

 
libraryDependencies += "org.jsoup" % "jsoup" % "1.6.3" 

libraryDependencies += "org.scalaj" %% "scalaj-http" % "0.3.16"



libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.0"
