package com.rock.util

import java.io.FileOutputStream
import java.io.File

object FileUtil {
	def createFile(fileName:String, data:Array[Byte]){
	  val file = new File(fileName)
	  val dir = file.getParentFile()
	  if(!dir.exists())
	    dir.mkdirs()
	  val out = new FileOutputStream(fileName)
	  out.write(data)
	  out.close()
	}
}