package com.rock.crawl
import scala.concurrent._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import org.omg.CORBA.Object
import scala.collection.mutable.ArrayBuffer
import org.jsoup.Jsoup
import akka.actor.ActorSystem
import akka.actor.Props
import com.rock.crawl.message.CrawlStart
import com.rock.crawl.actor.CrawlMasterActor
import akka.actor.ActorRef

class Crawler {
  private var _filter: String => Boolean = (url: String) => true
  private var _source: ArrayBuffer[String] = ArrayBuffer.empty[String]
  private var _processor: (String, Array[Byte]) => Unit = (url: String, data: Array[Byte]) => {}
  private var system: ActorSystem = null
  private var crawlMaster: ActorRef = null
  this.init

  private def init() {
    system = ActorSystem("Crawler")
    // create and start the actor
    crawlMaster = system.actorOf(Props(new CrawlMasterActor(this)), name = "CrawlMaster")
    
    
  }

  /**
 * @param filter
 * @return
 */
def filter(filter: String => Boolean) = {
    this._filter = filter
    this
  }
  def filter = {
    this._filter
  }
  def source(source: ArrayBuffer[String]) = {
    this._source = source
    this
  }
  def source = {
    this._source
  }

  /**
 * @param processor 
 * @return
 */
def processor(processor: (String, Array[Byte]) => Unit) = {
    this._processor = processor
    this
  }
  def processor = {
    this._processor
  }
  def start {
    crawlMaster ! CrawlStart
   
  }
  def shutdown{
    system.shutdown
  }
}
 