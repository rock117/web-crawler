package com.rock.crawl

trait CrawlFilter {
	def accept(url:String):Boolean
}