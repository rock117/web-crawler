package com.rock.crawl.message

case class CrawlLinks(urls:Seq[String] = Seq.empty[String])