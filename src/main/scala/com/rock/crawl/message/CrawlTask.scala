package com.rock.crawl.message

case class CrawlTask(url:String, processor:(String, Array[Byte]) => Unit)