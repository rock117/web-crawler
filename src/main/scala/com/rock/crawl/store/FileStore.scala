package com.rock.crawl.store

import com.rock.util.FileUtil
import java.net.URL
import scala.collection.immutable.LinearSeq

class FileStore(var dir: String) {
  def process(url: String, data: Array[Byte]) {
    val uri = new URL(url)
    val host = uri.getHost()
    val path = uri.getPath()
    val fileName = this.dir + "/" + host + makeLegalFilePath(path)
    if(makeLegalFilePath(path).equals(".html")){
      println("url is:"+url)
      System.exit(1)
    }
    FileUtil.createFile(fileName, data)
  }

  private def makeLegalFilePath(path: String) = {
    val res = path match {
      case null => "/index.html"
      case _ if path.trim().isEmpty() => "/index.html"
      case _ if !path.startsWith("/") => "/"+path
      case _ => path
    }
    if(res.contains(".")) res else res+".html"
  }
  
  
  
}
object FileStore {
  def apply(dir: String) = new FileStore(dir)
}