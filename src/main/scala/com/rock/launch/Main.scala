package com.rock.launch

import com.rock.crawl.Crawler
import scala.collection.mutable.ArrayBuffer
import com.rock.crawl.store.FileStore
import java.net.URL

object Main extends App {
  println("crawl begin")
  new Crawler().source(ArrayBuffer("http://money.163.com/stock/"))
    .processor(FileStore("d:/crawl-sites").process)
    .filter((url: String) => {
      url.contains("163.com")

    }).start

}