package com.rock.cfg

object FileConfig {
  private var storePath = "D:/crawl sites"
  def getStorePath = {
    storePath
  }

  def setStorePath(storePath: String) {
    this.storePath = storePath
  }
}