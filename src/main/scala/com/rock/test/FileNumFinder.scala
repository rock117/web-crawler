package com.rock.test

import akka.actor.Actor
import akka.actor.PoisonPill

object FileNumFinder extends App {

}

class FileNumSummary extends Actor {
  var fileToFind = 0
  var totalFile = 0
  def receive = {

    case FileResult(fileNum) => totalFile += fileNum
    case _ => if (fileToFind == 0) self ! PoisonPill
  }
}
class Worker extends Actor {
  def receive = {
    case FileInfo(fileName) => ""
  }
}
case object StartFind
case class FileInfo(fileName: String)
case class FileResult(fileNum: Int)