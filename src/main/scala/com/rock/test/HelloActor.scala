package com.rock.test

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props
import scala.collection.mutable.ArrayBuffer
import org.jsoup.Jsoup
import com.rock.crawl.store.FileStore
import scala.collection.mutable.Set
import java.util.HashSet

class HelloActor extends Actor {
  def receive = {
    case "hello" =>
      println("hello received")
      Thread.sleep(1111*3)
    case _ =>
      println("back at you")
      Thread.sleep(11111)
    //      context.stop(self)
  }

  override def postStop {
    println("stop")
  }
  override def preStart {
    println("start")
  }
}
object Main extends App {
  // an actor needs an ActorSystem
  val system = ActorSystem("HelloSystem")
  // create and start the actor
  val helloActor = system.actorOf(Props[HelloActor], name = "helloactor")
  // send the actor two messages
  helloActor ! "hello"
  helloActor ! "buenos dias"
  // shut down the system
  Thread.sleep(11111)
  system.shutdown

  //  val resp = Jsoup.connect("http://www.netbian.com/").execute()
  //    val code = resp.statusCode()
  //    val data = resp.bodyAsBytes()
  //    val doc = resp.parse()
  //    
  //
  //    val links = doc.select("a[href]");
  //    val media = doc.select("[src]");
  //    val imports = doc.select("link[href]");
  //    val size = media .size()
  //    for(i <- 0 to size - 1){
  //      println(media .get(i).attr("src"))
  //    }
  //  
  //  

  //  println("http://www.netbian.com/".matches(".*\\bwww.netbian.com\\b.*"))
  //  println((Seq(1,2)++Seq(3,4)++Seq(5,6)).mkString(","))

  val set = new HashSet[Int]()
  set.add(1)
  set.add(22)
  println(set)

}