package com.rock.http
import org.apache.commons.httpclient.Header
case class HttpResponse(status: Int, data: Array[Byte], headers: Array[Header] = Array.empty[Header]) {
  def rawHeader = {
    var str = ""
    for (header <- headers) {
      str = str + header.getName() + ":" + header.getValue() + "\n";
    }
    str
  }
}