package com.rock.tmp

import akka.actor.Actor
import akka.actor.ActorRef

class SizeCollector extends Actor{
	var toProcessFileNames = List.empty[String]
	var fileProcessors = List.empty[ActorRef]
	var pendingNumberOfFilesToVisit = 0L
	var totalSize = 0L
	val start = System.nanoTime()
	
	def sendAFileToProcess = {
	  if(!toProcessFileNames.isEmpty && !fileProcessors.isEmpty){
	    fileProcessors.head ! FileToProcess(toProcessFileNames.head)
	    fileProcessors = fileProcessors.tail
	    toProcessFileNames = toProcessFileNames.tail
	  }
	}
	
	def receive = {
	  case RequesstAFile => 
	    sendAFileToProcess
	    
	  case FileToProcess(fileName) =>
	    toProcessFileNames  = fileName :: toProcessFileNames 
	    pendingNumberOfFilesToVisit += 1
	    sendAFileToProcess
	    
	  case FileSize(size) =>
	    totalSize += size
	    pendingNumberOfFilesToVisit -= 1
	    if(pendingNumberOfFilesToVisit == 0)
	    {
	      val end = System.nanoTime()
	      println("total size is "+totalSize)
	      println("time taken is "+(end-start)/1.0e9)
	      //shutdown all actor
	    }
	}
}