package com.rock.tmp

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props

object ConcurrentFileSizeWAkkaMain extends App {
  val system = ActorSystem("Test")
  val sizeCollector = system.actorOf(Props[SizeCollector])
  sizeCollector ! FileToProcess("")
  
}