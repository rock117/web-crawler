package com.rock.tmp

import akka.actor.Actor
import akka.actor.ActorRef
import java.io.File

class FileProcessor(sizeCollector:ActorRef) extends Actor{

  override def preStart = registerToGetFile
  
  def registerToGetFile = {
    sizeCollector ! RequesstAFile
  }
  
  def receive = {
    case FileToProcess(fileName) =>
      val file = new File(fileName)
      var size = 0L
      if(file.isFile())
        size = file.length()
      else{
        val childeren = file.listFiles()
        if(childeren !=null){
          for(child <- childeren){
            if(child.isFile())
              size+=child.length()
            else{
              sizeCollector ! FileToProcess(child.getPath())
            }
          }
        }
      }
      sizeCollector ! FileSize(size)
      registerToGetFile
  }
}