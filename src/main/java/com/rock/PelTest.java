package com.rock;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PelTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService es = Executors.newFixedThreadPool(1);

        List<Callable<String>> tasks = new ArrayList<Callable<String>>();
        for (int i = 0; i < 3; i++) {
            tasks.add(new Callable<String>() {

                @Override
                public String call() throws Exception {
                    System.out.println("thread " + Thread.currentThread().getId() + " start...");
                    Thread.sleep(1000 * 10);
                    return "123";
                }
            });
        }
        List<Future<String>> futures = es.invokeAll(tasks);
        System.out.println("get all futures");
        for (Future<String> future : futures) {
            System.out.println("result:" + future.get());
        }
    }

}
