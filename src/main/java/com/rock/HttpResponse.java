package com.rock;

import java.util.*;

 
public class HttpResponse {
	public int status = 200;
	public Map<String, String> headers = new HashMap<String, String>();
	public byte[] data;
	public HttpResponse(byte[] data){
		this.data = data;
	}
}
