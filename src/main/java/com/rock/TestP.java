package com.rock;

import org.apache.commons.httpclient.protocol.DefaultProtocolSocketFactory;
import org.apache.commons.httpclient.protocol.Protocol;

public class TestP {

    public static void main(String[] args) {
        Protocol p = new Protocol("https", new DefaultProtocolSocketFactory(), 443);
        Protocol.registerProtocol("https", p);
    }
}
