package com.rock.test

import java.io.File
import java.io.FileOutputStream

object MyTest extends App {

  //  println("hello world")
  //  createProject("d:/k/kk")

  //  createNewFile("D:/test/test-scala/src/main/scala/rock/TestMain.scala")
  println("done")
  def createProject(rootPath: String) {
    createNewFile(s"$rootPath/project/build.properties")
    createNewFile(s"$rootPath/project/build.scala")
    createNewFile(s"$rootPath/project/plugins.bt")

    createNewFile(s"$rootPath/src/main/scala/ScalatraBootstrap.scala")
    createNewFile(s"$rootPath/src/main/scala/org/rock/http/MyScalatraServlet.scala")
    createNewFile(s"$rootPath/src/main/webapp/WEB-INF/views/hello-scalate.scaml")
    createNewFile(s"$rootPath/src/main/webapp/WEB-INF/layouts/default.scaml")
    createNewFile(s"$rootPath/src/main/webapp/WEB-INF/web.xml")

  }

  def mkdirs(dir: String) {
    val file = new File(dir);
    if (file.exists()) {
      return ;
    }
    file.mkdirs()
  }
  def createNewFile(path: String) {
    val file = new File(path)
    if (file.exists()) {
      return ;
    }
    mkdirs(file.getParent)
    new FileOutputStream(file).close()
  }

  val coll = List(1, 2, 3)
  val c2 = coll.partition((e: Int) => e == 1);

  //  val fn = 

}