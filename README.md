#web-crawler
一个简单的web爬虫，采用scala + akka实现，该爬虫可以指定过滤规则，存储处理（文件存储，数据库存储。。。），例子如下
new Crawler().source(ArrayBuffer("http://money.163.com/stock/"))
    .processor(FileStore("d:/crawl-sites").process)    
    .filter((url: String) => { 
      url.contains("163.com")
    }).start